#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <poll.h>

#define READBUF_SIZE 1024

int
set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                fprintf (stderr, "error %d from tcgetattr", errno);
                return -1;
        }

		switch(speed) {
			case 300:
				speed = B300;
				break;
			case 600:
				speed = B600;
				break;
			case 1200:
				speed = B1200;
				break;
			case 2400:
				speed = B2400;
				break;
			case 4800:
				speed = B4800;
				break;
			case 9600:
				speed = B9600;
				break;
			case 19200:
				speed = B19200;
				break;
			case 38400:
				speed = B38400;
				break;
			case 57600:
				speed = B57600;
				break;
			case 115200:
				speed = B115200;
				break;
			case 230400:
				speed = B230400;
				break;
			case 460800:
				speed = B460800;
			default:
				speed = B0;
				break;
		}

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 10;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                fprintf (stderr, "error %d from tcsetattr", errno);
                return -1;
        }
        return 0;
}

void
set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                fprintf (stderr, "error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
        tty.c_cc[VTIME] = 10;            // 0.5 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                fprintf (stderr, "error %d setting term attributes", errno);
}

int main(int argc, char** argv) {
	uint8_t count = 2;
	char* ppath = NULL;
	int baudrate = 4800;
	uint8_t daemonize = 0;
	int tty_fd, opt;
	int *slaves;
	
	while ((opt = getopt(argc, argv, "c:p:b:d")) != -1) {
        switch (opt) {
        case 'c':
            count = atoi(optarg);
            break;
		case 'b':
			baudrate = atoi(optarg);
			break;
		case 'd' :
			daemonize = 1;
			break;
        default: /* '?' */
            fprintf(stderr, "Usage: %s -c count [-b baudrate] <tty path>\n",
                    argv[0]);
            exit(EXIT_FAILURE);
        }
    }

	if (optind >= argc) {
            fprintf(stderr, "Usage: %s -c count [-b baudrate] <tty path>\n",
                    argv[0]);
		exit(EXIT_FAILURE);
	}

	ppath = argv[optind];
	slaves = malloc(sizeof(int) * count);

	tty_fd = open(ppath, O_RDONLY | O_NOCTTY);
	if( tty_fd < 0 ) {
		fprintf(stderr, "can't open %s tty\n", ppath);
		exit(EXIT_FAILURE);
	}

	set_interface_attribs (tty_fd, baudrate, 0);
	struct pollfd p_in, *p_out;
	p_out = malloc(sizeof(struct pollfd) * count);
	for( size_t i = 0; i < count; i++ ) {
		char* slavename = NULL;
		char slavepath[256];
		slaves[i] = open("/dev/ptmx", O_RDWR | O_SYNC);
		if( slaves[i] < 0 ) {
			fprintf(stderr, "can't open pty for slave %zu\n", i);
			exit(EXIT_FAILURE);
		}
		grantpt(slaves[i]);
		slavename = ptsname(slaves[i]);
		if( slavename == NULL ) {
			fprintf(stderr, "can't get name for slave %zu\n", i);
			exit(EXIT_FAILURE);
		}
		snprintf(slavepath, 255, "/dev/ttyDUP%zu", i);
		unlink(slavepath);
		if( symlink(slavename, slavepath) < 0 ) {
			fprintf(stderr, "can't make symlink %s for slave %s\n", slavepath, slavename);
			exit(EXIT_FAILURE);
		}
		unlockpt(slaves[i]);

		p_out[i].fd = slaves[i];
		p_out[i].events = POLLOUT;
	}
	
	char readbuf[READBUF_SIZE];
	size_t offset = 0;
	p_in.fd = tty_fd;
	p_in.events = POLLIN;

		

	while(1) {
		int ret;

		if( READBUF_SIZE <= offset ){
			offset = 0;
			memset(readbuf, 0, READBUF_SIZE);
		}

		ret = poll(&p_in, 1, 500); // 500 ms timeout

		if( ret > 0 && p_in.revents & POLLIN ) {
			ssize_t len = read(p_in.fd, readbuf, READBUF_SIZE);

			if( len <= 0 ) {
				// handle error
				continue;
			}

			ret = poll(p_out, count, 500); // 500 ms timeout
			if( ret <= 0 )
				continue;
			for( ssize_t i = 0; i < count; i++) {
				tcflush(slaves[i], TCIOFLUSH);
				if( p_out[i].events & POLLOUT ) {
					write(slaves[i], readbuf, len);
					fsync(slaves[i]);
				}
			}
		}
	}
}
